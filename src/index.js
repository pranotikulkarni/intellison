import { render } from 'react-dom';
import { Router, Route, browserHistory } from 'react-router';

import Home from 'pages/home';
import Signup from 'pages/signup';
import Login from 'pages/login';
import About from './pages/about';

render ((
	<Router history={browserHistory}>
		<Route path="/" component={Home} />
		<Route path="signup" component={Signup} />
		<Route path="login" component={Login} />
		<Route path="about" component={About} />
	</Router>
), document.getElementById('root'));