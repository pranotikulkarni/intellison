import { Link } from 'react-router';

export default class Footer extends React.Component {



  render() {
    let st = this.state;
    return (
      <nav className="navbar has-shadow">

          <div className="navbar-brand">
            <Link className="navbar-item" to="/">
              <span>Copyright © 2017 Intellison LLC</span>
	          </Link>

            <div className="navbar-burger burger" data-target="navbarMenu">
              <span></span>
              <span></span>
              <span></span>
              <span></span>
            </div>
          </div>


          <div className="navbar-end" id="navbarMenu">
            <Link className="navbar-item" to="#">
              Twitter
	          </Link>

            <Link className="navbar-item" to="#">
              Facebook
	          </Link>

            <Link className="navbar-item" to="#">
              Google
	          </Link>

            <Link className="navbar-item" to="#">
              LinkedIn
	          </Link>

          </div>
        </nav>
    )
  }
}