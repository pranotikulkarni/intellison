import { Link } from 'react-router';


export default class Header extends React.Component {

	render() {
		return (
			<nav className="navbar has-shadow">

				<div className="navbar-brand">
					<Link className="navbar-item" to="/">
						Intellison
					</Link>
				
				<div className="navbar-burger burger" data-target ="navbarMenu">
  					<span></span>
  					<span></span>
  					<span></span>
					<span></span>
				</div>
				</div>


				<div className="navbar-end" id = "navbarMenu">
					<Link className="navbar-item" to="/">
						Home
					</Link>

					<Link className="navbar-item" to="about">
						About
					</Link>

					<Link className="navbar-item button m10" to="signup">
						Register
					</Link>

					<Link className="navbar-item button m10" to="login">
						Login
					</Link>

				</div>
			</nav>
		)
	}
}