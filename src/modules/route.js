export default class Route {

	constructor(baseUrl) {
		this.baseUrl = baseUrl;
	}

	post(url) {
		return $.post(this.baseUrl + url);
	}

	post(url, data) {
		return $.post(this.baseUrl + url, data); //JSON.stringify(data) ?
	}
}