var Validator = module.exports = {

        isName: function(value){
            return value.match(/^[a-z0-9_-]{3,15}$/);
        },

        isUsername: function(value){
            return (value.length < 10 && value.match(/[a-zA-Z_]\w*/));
        },
    
        isEmail: function(value) {
            return value.match(/^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i);
        },

        isPassword: function(value) {
            return (value.length < 9 && value.match(/[a-zA-Z_][\w]*/));
        },

        isConfirm: function(value1,value2) {
            return (value1===value2);
        },
    }
