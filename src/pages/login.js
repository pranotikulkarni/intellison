import Header from '../components/header';
import { browserHistory } from 'react-router';
import Validator from '../modules/validator';

export default class Login extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            email: null,
            password: null,
            emailError: null,
            passwordError:null,
            emailErr:null,
            passwordErr:null,
            loading: null,
            error: null,
        };
    }

    render() {
        let st = this.state;
        return (
            <section className="hero is-fullheight hero-is-dark">
                <Header />
                <div className="hero-body">
                    <div className="container">
                        <div className="columns is-vcentered">
                            <div className="column is-4 is-offset-4">
                                <div className="box">

                                    <div className="login-form">
                                        <label className="label m0 mt15">Email</label>
                                        <p className="control has-icon has-icon-right">
                                            <input className={'input' + (st.emailError !== null ? ' is-danger' : '')} type="text" placeholder="jdoe@example.org" value={st.email} onChange={this.handleEmailChange} errorText={[st.emailError,st.emailErr]} />
                                        </p>
                                        <label className="label m5 red" style={{ "minHeight": "24px" }}>{[st.emailError,st.emailErr]}</label>

                                        <label className="label m0 mt15">Password</label>
                                        <p className="control has-icon has-icon-right">
                                            <input className={'input' + (st.passwordError !== null ? ' is-danger' : '')} type="password" placeholder="●●●●●●●" value={st.password} onChange={this.handlePasswordChange} errorText={st.passwordError} />
                                        </p>
                                        <label className="label m5 red" style={{ "minHeight": "24px" }}>{[st.passwordError,st.passwordErr]}</label>

                                        <p className="control">
                                            <button className='button is-primary is-outlined is-large is-fullwidth mt15 mb20 ' onClick={l => this.onkeypress(l)}>Login</button>
                                        </p>
                                        <label className="label m5 red" style={{ "minHeight": "24px" }}>{st.error}</label>

                                    </div>
                                    <div className="section forgot-password">
                                        <p className="has-text-centered">
                                            <a href="#"> Forgot password </a>
                                            |
                                            <a href="#"> Need help? </a>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        )
    }
    handlePasswordChange = (pwd) => {
        this.setState({ password: pwd.target.value });
    }

    handleEmailChange = (em) => {
        this.setState({ email: em.target.value });
    }

    validate = () => {
        let errors = 0;
        let st = this.state;
        const err = {};


        if (st.email === null || st.email === undefined) {
            errors++;
            this.setState({ emailError: 'Email required' });
        }

        if (st.password === null || st.password === undefined) {
            errors++;
            this.setState({ passwordError: 'Password required' });
        }


        if (st.email) {
            if (!Validator.isEmail(st.email)) {
                errors++;
                this.setState({ emailErr: 'Invalid email' });
            }
        }

        if (st.password) {
            if (!Validator.isPassword(st.password)) {
                errors++;
                this.setState({ passwordErr: 'Invalid password' });
            }
        }


        return errors;
    };

    resetErrors() {
        this.setState({
            emailError: null,
            passwordError: null,
            emailErr:null,
            passwordErr:null
        })
    }


    onkeypress = l => {
        this.setState({ error: null, loading: ' is-loading' });
        // this.props.onSubmit(this.state);
        this.resetErrors();
        let errors = this.validate();
        if (errors > 0) {
            this.setState({ error: "Please correct errors" }); 
            return;
        }
        else {
            this.setState({ error: "Submitted successfully" });
            return;
        }
        let st = this.state;
        this.setState({ error: null, loading: null });
    }

    /*onpointercancel = e => {
        // Show a confirm popup ?
        browserHistory.push('/');
    }*/

}