import React, { Component } from 'react';
import Header from '../components/header';
import Footer from '../components/footer';


export default class About extends Component {

  render() {
    return (
      <section className="hero is-fullheight hero-is-dark">
      <Header/>
      <div id="header">
      <div className="container">

        <div id="logo">
          <h1 style={{textAlign: 'center'}}><a href="/">Intellison LLC</a></h1><br />
          <h2 style={{textAlign: 'center'}}><a href='#'>Software-Services-Training</a></h2>
        </div>
        
        <div id="banner">
        <header>
        <p style={{fontSize: "30px", fontWeight: "bold", textAlign: 'center'}}>Cisco Certification Training</p>
        </header>
        </div>

        <div id="banner">
        <header>
          <h2 style={{textAlign: 'center'}}><a href='#'>Training Services</a></h2>
          <p style={{"fontSize": "30px", textAlign: 'center'}}>CCNA - Cisco Certified Network Associate</p>
        </header>

            <header>
            <p style={{"fontSize": "30px", textAlign: 'center'}}>CCNP - Cisco Certified Network Professional</p>
            </header>

            <header>
            <p style={{"fontSize": "30px", textAlign: 'center'}}>CCIE - Cisco Certified Internetwork Expert</p>
            </header>

        </div>

      </div>

    </div>
    <Footer />
    </section>
     );
   }
 }