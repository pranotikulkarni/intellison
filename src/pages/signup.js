
import { browserHistory } from 'react-router';

import Header from '../components/header';

import Validator from '../modules/validator';
import Route from '../modules/route';


//  import validator from 'validator';

export default class Signup extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            name: null,
            username: null,
            email: null,
            password: null,
            confirm: null,
            NameError: null,
            userNameError: null,
            emailError: null,
            confirmError: null,
            passwordError: null,
            loading: null,
            error: null,
        };
    }



    render() {
        let st = this.state;

        return (
            <section className="hero is-fullheight hero-is-dark">
            <Header />
            <div className="hero-body">
                <div className="container">
                    <div className="columns is-vcentered">
                        <div className="column is-6 is-offset-3">
                            
                            <div className="box">

                                    <label className="label m0">Name</label>
                                    <p className="control">
                                        <input className={'input' + (st.NameError !== null ? ' is-danger' : '')} type="text" placeholder="John Doe" value={st.name} onChange={this.handleNameChanged} errorText={st.NameError} />
                                    </p>



                                    <label className="label m0 mt15">Username</label>
                                    <p className="control has-icons-left">
                                        <input className={'input' + (st.userNameError !== null ? ' is-danger' : '')} type="text" placeholder="johndoe" value={st.username} onChange={this.handleUserNameChanged} errorText={st.userNameError} />
                                        <span className="icon is-small is-left">
                                            <i className="fa fa-user" aria-hidden="true"></i>
                                        </span>
                                    </p>

                                    <label className="label m0 mt15">Email</label>
                                    <p className="control has-icons-left">
                                        <input className={'input' + (st.emailError !== null ? ' is-danger' : '')} type="text" placeholder="jdoe@example.org" value={st.email} onChange={this.handleEmailChange} errorText={st.emailError} />
                                        <span className="icon is-small is-left">
                                            <i className="fa fa-envelope" aria-hidden="true"></i>
                                        </span>
                                    </p>

                                    <hr />

                                    <label className="label m0">Password</label>
                                    <p className="control">
                                        <input className={'input' + (st.passwordError !== null ? ' is-danger' : '')} type="password" placeholder="●●●●●●●" value={st.password} onChange={this.handlePasswordChange} errorText={st.passwordError} />
                                    </p>

                                    <label className="label m0 mt15">Confirm Password</label>
                                    <p className="control">
                                        <input className={'input' + (st.confirmError !== null ? ' is-danger' : '')} type="password" placeholder="●●●●●●●" value={st.confirm} onChange={this.handleConf} errorText={st.confirmError} />
                                    </p>

                                    <hr />

                                    <label className="label m5 red" style={{ "minHeight": "24px" }}>{st.error}</label>

                                    <p className="control">
                                        <button className='button is-primary is-outlined m5 ' onClick={e => this.onSubmit(e)}>Register</button>
                                        <button className="button is-dark is-outlined m5" onClick={e => this.onpointercancel(e)}>Cancel</button>
                                    </p>
                                </div>
                                <p className="has-text-centered">
                                    <a href="login"> Login </a>
                                    |
                                    <a href="#"> Need help?</a>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        )
    }


    //change handler 
    handleNameChanged = (n) => {
        this.setState({ name: n.target.value });
    }

    handleUserNameChanged = (u) => {
        this.setState({ username: u.target.value });
    }


    handlePasswordChange = (evt) => {
        this.setState({ password: evt.target.value });
    }

    handleEmailChange = (e) => {
        this.setState({ email: e.target.value });
    }

    handleConf = (c) => {
        this.setState({ confirm: c.target.value });
    }

    validate = () => {
        let errors = 0;
        let st = this.state;
        const err = {};

        if (st.name === null || st.name === undefined) {
            errors++;
            this.setState({ NameError: 'required' });
        }

        if (st.name) {
            if (!Validator.isName(st.name)) {
                errors++;
                this.setState({ NameErr: 'invalid' });
            }
        }


        if (st.username === null || st.username === undefined) {
            errors++;
            this.setState({ userNameError: 'required' });
        }

        if (st.username) {
            if (!Validator.isUsername(st.username)) {
                errors++;
                this.setState({ userNameError: 'invalid' });
            }
        }

        if (st.email === null || st.email === undefined) {
            errors++;
            this.setState({ emailError: 'required' });
        }

        if (st.password === null || st.password === undefined) {
            errors++;
            this.setState({ passwordError: 'required' });
        }

        if (st.confirm === null || st.confirm === undefined) {
            errors++;
            this.setState({ confirmError: 'required' });
        }

        if (st.email) {
            if (!Validator.isEmail(st.email)) {
                errors++;
                this.setState({ emailError: 'invalid email' });
            }
        }

        if (st.password) {
            if (!Validator.isPassword(st.password)) {
                errors++;
                this.setState({ passwordError: 'invalid password' });
            }
        }

        if (st.confirm) {
            if (!Validator.isConfirm(st.password, st.confirm)) {
                errors++;
                this.setState({ confirmError: 'invalid password match' });
            }
        }


        return errors;
    };

    resetErrors() {
        this.setState({
            NameError: null,
            userNameError: null,
            emailError: null,
            confirmError: null,
            passwordError: null,
        })
    }


    onSubmit = e => {
        this.setState({ error: null, loading: ' is-loading' });
        // this.props.onSubmit(this.state);
        this.resetErrors();
        let errors = this.validate();
        if (errors > 0) {
            this.setState({ error: "Please correct errors" }); //how to append errorText after please correct errors//to-do
            return;
        }
        else {
            this.setState({ error: "Submitted successfully" });
            return;
        }
        let st = this.state;
        this.setState({ error: null, loading: null });
    }

    onpointercancel = e => {
        // Show a confirm popup ?
        browserHistory.push('/');
    }

}







