import Header from '../components/header';
import Footer from '../components/footer';

export default class Home extends React.Component {

	render() {
		return (
			<section className="hero is-fullheight hero-is-dark">
				<Header />
				<div id="header">
        <div className="container">

          <div id="logo">
            <h1><a href="/">Intellison LLC</a></h1><br />
            <h2><a href='#'>Software-Services-Training</a></h2>
          </div>
          
          <div id="banner">
          <header>
          <p style={{fontSize: "30px", fontWeight: 'bold', textAlign: 'center'}}>Cisco Certification Training</p>
          <p style={{"fontSize": "30px"}}>An affordable and facilitated fast track to mastering Cisco Technologies</p>
          </header>
          </div>

          <nav id="nav">
            <ul>
              <li><a href="left-sidebar.html">Services</a></li>
              <li><a href="no-sidebar.html">Portfolio</a></li>
              <li><a href="contact.html">Contact</a></li>
            </ul>
          </nav>
          <div id="banner">
          <header>
            <h2>Training Services</h2>
            <p style={{fontSize: '30px', textAlign: 'center'}}>CCNA - Cisco Certified Network Associate</p>
          </header>
      

              <p style={{padding: "10px", textAlign: "left",letterSpacing: "2px"}}>The working knowledge of CCNA routing and switching is foundational and required for network engineers to understand industry standard networking technologies.<br />

              The CCNA program prepares the candidate for the following two-module certification exam:<br /><br />

              <i className="fa fa-circle" aria-hidden="true"></i> 100-105 Interconnecting Network Devices (ICND1)<br />
              <i className="fa fa-circle" aria-hidden="true"></i> 200-105 Interconnecting Network Devices (ICND2)<br /><br />
              To prepare for the written and lab exams, we supply the candidate with:<br /><br />

              <i className="fa fa-circle" aria-hidden="true"></i> Routine classes with a well structured curriculum<br />
              <i className="fa fa-circle" aria-hidden="true"></i> Online lab access<br />
              <i className="fa fa-circle" aria-hidden="true"></i> Textbooks and online materials for both exams<br /></p>

              <header>
              <p style={{fontSize: "30px", textAlign: 'center'}}>CCNP - Cisco Certified Network Professional</p>
              </header>

              <p style={{padding: "10px", textAlign: "left", letterSpacing: "2px"}}>The CCNP Routing and Switching program is designed for network engineers who aspire to plan, implement, verify, and troubleshoot local and wide-area networks.<br />

              The CCNP R&S program prepares the candidate for the following modules of the exam:<br /><br />

              <i className="fa fa-circle" aria-hidden="true"></i> 300-101 ROUTE (Implementing IP Routing) V2.0<br />
              <i className="fa fa-circle" aria-hidden="true"></i> 300-115 SWITCH (Implementing switched networks) V2.0<br />
              <i className="fa fa-circle" aria-hidden="true"></i> 300-135 TSHOOT (Troubleshooting and Maintaining Networks) V2.0<br /><br />
              To prepare for the written and lab exams, we supply the candidate with:<br /><br />

              <i className="fa fa-circle" aria-hidden="true"></i> Routine classes with a well structured curriculum<br />
              <i className="fa fa-circle" aria-hidden="true"></i> Online lab access<br />
              <i className="fa fa-circle" aria-hidden="true"></i> Textbooks and online materials for the exam<br /></p>

              <header>
              <p style={{fontSize: "30px", textAlign: 'center'}}>CCIE - Cisco Certified Internetwork Expert</p>
              </header>

              <p style={{padding: "10px", textAlign: "left", letterSpacing: "2px"}}>The CCIE program is designed for senior network engineers and architects who aspire to design, deploy, and maintain large complex networks for service providers and enterprises.<br />

              The CCIE program is customized for each candidate based on their current knowledge.<br />

              The CCIE program prepares the candidate for the following modules of the exam:<br /><br />

              <i className="fa fa-circle" aria-hidden="true"></i> 400-101 Routing and Switching (Written)<br />
              <i className="fa fa-circle" aria-hidden="true"></i> 400-101 Lab Exam<br /><br />

              To prepare for the written and lab exams, we supply the candidate with:<br /><br />

              <i className="fa fa-circle" aria-hidden="true"></i> Routine classes with a well structured curriculum<br />
              <i className="fa fa-circle" aria-hidden="true"></i> Online lab access<br />
              <i className="fa fa-circle" aria-hidden="true"></i> Textbooks and online materials for the exam<br />
              <i className="fa fa-circle" aria-hidden="true"></i> Dedicated equipment to configure and troubleshoot<br /></p>

          </div>

        </div>

      </div>
	  <Footer/>
			</section>
		)
	}
}