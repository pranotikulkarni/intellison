'use strict';
var path = require('path');
var webpack = require('webpack');

var config = {
	devtool: 'cheap-module-source-map',
	entry: path.resolve(__dirname, 'src/index.js'),
	output: {
		path: path.join(__dirname, 'public'),
		filename: 'bundle.js',
		publicPath: '/'
	},
	plugins: [
		new webpack.ProvidePlugin({
			"React": "react",
		}),
		new webpack.DefinePlugin({
			'process.env': {
				'NODE_ENV': JSON.stringify('production')
			}
		}),
	],
	resolve: {
		modules: [path.join(__dirname, "src"), "node_modules", "src", "bower_components"]
	},
	devServer: {
		contentBase: "public",
		historyApiFallback: true
	},
	module: {
		loaders: [
			{
				test: /\.js?$/,
				exclude: /node_modules/,
				loader: 'babel-loader',
				query: {
					presets: ["react", "es2015", "stage-0"]
				}
			},
			{
				test: /\.scss$/,
				loaders: ['style-loader', 'css-loader', 'sass-loader']
			}
		]
	}
};

module.exports = config;